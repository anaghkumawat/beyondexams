import React from 'react'
import Avatar from '../assets/avatar_me.jpg'

const ProfileIcon = () => {
  return (
    <div className='flex items-center flex-col bg-[#f1edff] rounded-[0_22px_22px_0] relative'>
      <div className='flex flex-row cursor-pointer my-auto mx-2 rounded-[5px]'>
        <div className='border-[0.1em] border-solid border-[rgb(102,70,231)] rounded-full p-[0.15em]'>
        <img className='h-[25px] w-[25px] rounded-full m-auto relative bg-[50%] bg-[cover] bg-no-repeat' alt="Avatar" src={Avatar}/>
        </div>
      </div>  
    </div>
  )
}

export default ProfileIcon