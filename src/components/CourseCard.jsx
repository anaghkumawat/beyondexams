import React from 'react'
import { DotsVerticalIcon } from "@heroicons/react/solid";

const CourseCard = (props) => {
  return (
    <div>
        <div className='my-[15px] mx-auto w-[190px] h-[290px] p-[5px] shadow-[0_1px_5px_rgba(0,0,0,.25)] rounded-[12px] bg-white cursor-pointer hover:shadow-[0_1px_15px_rgba(0,0,0,.25)]'>
        <img className='w-[180px] h-[194px] bg-[50%] bg-[cover] rounded-[6px_6px_0_0] bg-no-repeat' alt="CardImage" src={props.src} />

        <div className='flex justify-between pl-[8px] my-[10px]'>
          <span className='overflow-hidden whitespace-nowrap text-ellipsis'>{props.title}</span>
          <DotsVerticalIcon  className='h-6'/>
        </div>

        <div className='mt-[20px] px-[8px]'>
        <span className='text-[10px]'>{props.count} Courses</span>
        </div>
        </div>
    </div>
  )
}

export default CourseCard