import React, { useState } from "react";
import { MenuIcon, SearchIcon, ChevronDownIcon } from "@heroicons/react/solid";
import Logo from "../assets/beyond-exams-logo.svg";
import ProfileIcon from "./ProfileIcon";
import { useNavigate } from "react-router-dom";

const Header = () => {
  const navigate = useNavigate();

  const [isSearchActive, setIsSearchActive] = useState(false);

  const handleSearchClick = () => {
    setIsSearchActive(!isSearchActive);
  };

  const goToBrowse = () => {
    navigate("/learn-a-skill");
  };

  return (
    <header className="h-[68px] py-0 px-5 flex items-center justify-between bg-[#fff] shadow-[0_0_10px_1px_rgba(0,0,0,.2)] border-b border-solid border-[rgba(0,0,0,.2)] sticky top-0 z-[102]">
      <div className="flex items-center cursor-pointer">
        <MenuIcon className="h-8 lg:hidden" />
        <a href="/">
          <img className="h-[67px] w-[107px] bg-[50%]" alt="Logo" src={Logo} />
        </a>
      </div>

      <span
        className="ml-[10px] py-2 px-[15px] text-[13px] font-medium cursor-pointer"
        onClick={goToBrowse}
      >
        Courses
      </span>

      <div className="flex grow shrink">
        <div
          className={`flex items-center justify-start ml-5 grow shrink h-[55px] rounded-[25px_0_0_25px] w-full bg-[#f1edff] cursor-pointer relative ${
            isSearchActive ? "bg-white" : ""
          }`}
        >
          <div
            className={`w-[55px] h-[55px] relative ${
              isSearchActive ? "relative w-full h-full duration-[.3s]" : ""
            }`}
          >
            <div
              className={`flex items-center absolute left-[-2px] rounded-[40px] bg-transparent top-0 w-full ${
                isSearchActive
                  ? "w-full bg-white shadow-[0_4px_9px_rgba(0,0,0,.25)] rounded-[40px]"
                  : ""
              }`}
            >
              <input
                className={`border-none outline-none w-0 placeholder:text-gray-500 text-[16px] ${
                  isSearchActive ? "w-full ml-[20px] mr-[5px] grow shrink" : ""
                }`}
                type="text"
                placeholder="What do you want to learn today?"
              />
              <button
                className="flex items-center bg-[#6646e7] w-[55px] h-[55px] min-w-[55px] min-h-[55px] rounded-full justify-center"
                onClick={handleSearchClick}
              >
                <SearchIcon className="h-[22px] text-[#fff]" />
              </button>
            </div>
          </div>
          <span
            className={`ml-4 text-gray-600 block ${
              isSearchActive ? "hidden" : ""
            }`}
          >
            What do you want to learn today?
          </span>
          <div
            className={`grow shrink justify-center self-end ${
              isSearchActive ? "hidden" : ""
            }`}
          ></div>
          <div className="flex items-center mr-[35px] gap-[35px]">
            <div
              className={`flex items-center ml-[10px] text-[13px] font-medium cursor-pointer ${
                isSearchActive ? "hidden" : ""
              }`}
            >
              Our services
              <ChevronDownIcon className="h-6" />
            </div>
          </div>
        </div>
        <ProfileIcon />
      </div>
    </header>
  );
};

export default Header;
