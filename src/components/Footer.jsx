import React from "react";

const Footer = () => {
  return (
    <div>
      <div className="bg-[#6646e7] w-full flex items-center flex-col relative rounded-[21px] mt-[60px] pb-[70px]">
        <div className="relative max-w-[1500px] w-full px-[28px]">
          <div className="absolute w-[270px] right-0 bottom-[-78px]">
            <img
              className="w-full h-auto"
              alt="Footer Illustration"
              src="https://beyondexams.org/static/media/home-footer-illustration-two.ba336c48.webp"
            />
          </div>
          <div className="mt-[60px] pl-[8px] w-full pr-[350px] flex justify-between flex-wrap text-white">
            <div className="mt-[20px] mr-[20px]">
              <h2 className="font-semibold mb-[10px]">Contact Us</h2>
              <div className="font-light">
                <p>5th floor, SBS Road</p>
                <p>Jamuna Sagar bldg</p>
                <p>Opp Sahakari Bhandar</p>
                <p>Near Sassoon Dock Colaba Bus Depot,</p>
                <p>Mumbai 400005</p>
                <div className="flex items-baseline mt-[60px]">
                  Social Links
                </div>
              </div>
            </div>
            <div className="flex flex-col mt-[20px] mr-[20px]">
              <h2 className="font-semibold mb-[10px]">About Us</h2>
              <a className="font-light mb-[2px]" href="/first">
                About Us
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Our team
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Progress Reports
              </a>
            </div>
            <div className="flex flex-col mt-[20px]">
              <h2 className="font-semibold mb-[10px]" href="/first">
                Resources
              </h2>
              <a className="font-light mb-[2px]" href="/first">
                Contact Us
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Recent Updates
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Get Involved
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Our Partners
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Why are we doing this?
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Roadmap
              </a>
              <a className="font-light mb-[2px]" href="/first">
                FAQ
              </a>
              <a className="font-light mb-[2px]" href="/first">
                Privacy Policy
              </a>
            </div>
            <div>
              <h3 className="mt-[25px] mb-[20px] font-semibold">
                Missed our updates?
                <br />
                Join our newsletter
              </h3>
              <button className="bg-white p-[8px] rounded-[10px] w-[125px] text-[#6646e7] font-medium text-[14px] cursor-pointer shadow-[0px_10px_20px_rgba(0, 0, 0, 0.15)]">
                Subscribe Now
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
