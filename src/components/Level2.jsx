import axios from 'axios'
import React from 'react' 
import { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom'
import CourseCard from './CourseCard'

const Level2 = () => {

const [searchParams] = useSearchParams()
const [topicsData, setTopicsData] = useState([])    

useEffect(() => {
    axios.get("https://api.beyondexams.org/api/v1/get_next_level_topics?"+searchParams)
    .then((response) => {
        setTopicsData(response.data.data.topics.data)
    })
},[searchParams])

  return (
    <div className='px-[20px] my-8 flex flex-col'>
        <div className='flex flex-wrap gap-4 justify-evenly'> {
                    topicsData.length > 0 ? topicsData.map((course,index) => (
                        <CourseCard src={course.image_url}
                            title={course.title}
                            count={course.num_topics}
                            key={index} />
            )): "Loading..."}
        </div>
    </div>
  )
          }

export default Level2