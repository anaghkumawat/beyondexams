import React from "react";
import HomeTopSection from "./HomeTopSection";

const Home = () => {
  return (
    <div>
      <HomeTopSection />
    </div>
  );
};

export default Home;
