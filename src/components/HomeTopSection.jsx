import React from "react";
import {
  ChevronDownIcon,
  SearchIcon,
  InformationCircleIcon,
} from "@heroicons/react/solid";

const HomeTopSection = () => {
  return (
    <div className="py-[15px] px-[20px] flex flex-col items-center">
      <div className="flex flex-col items-center w-full rounded-[20px] relative pb-[20px]">
        <div className="max-w-[1500px] w-full relative px-[28px]">
          <div className="mt-[75px] pl-[8px] max-w-[652px]">
            <p className="text-[#686868] pl-[3px]">A Non-Profit Organization</p>
            <h1 className="text-[40px] text-[#1d1d1d] font-medium leading-[73px]">
              "<span className="font-bold italic">Learn</span>
              <span> anything you want,</span>
              <br />
              <span className="font-bold italic">Teach</span> everything you
              know "
            </h1>

            <div className="flex justify-between mt-[20px] relative">
              <div className="relative inline-flex">
                <button className="cursor-pointer flex items-center justify-center font-semibold leading-none tracking-[.6px] whitespace-nowrap bg-[#6646e7] text-white p-[13px_20px_13px_29px] text-[16px] rounded-[32px] h-[50px]">
                  Courses
                  <ChevronDownIcon className="h-6 w-6 ml-[5px]" />
                </button>
              </div>
              <div className="flex bg-white rounded-[32px] ml-[20px] shadow-[0_10px_20px_rgba(0,0,0,.15)] w-full h-[50px]">
                <input
                  className="text-[16px] pl-[30px] pr-[80px] text-[#686868] rounded-[32px] w-full outline-none"
                  type="text"
                  placeholder="What do you want to learn today?"
                />
                <button className="w-[50px] h-[50px] min-w-[50px] min-h-[50px] rounded-full right-[-1px] bg-[#afadad] cursor-pointer flex justify-center items-center">
                  <SearchIcon className="h-[30px] w-[30px] mt-[5px] text-white" />
                </button>
              </div>
            </div>

            <div className="flex mt-[80px] pl-[8px]">
              <div className="flex items-center">
                <InformationCircleIcon className="h-6" />
                <span className="ml-[10px] cursor-pointer text-[#6646e7]">
                  Go through the tutorial
                </span>
              </div>
              <div className="flex items-center ml-[20px]">
                <InformationCircleIcon className="h-6" />
                <span className="ml-[10px] cursor-pointer text-[#6646e7]">
                  Create your own course
                </span>
              </div>
            </div>
          </div>

          <div className="max-w-[400px] bottom-0 absolute right-[60px]">
            <img
              className="w-full h-auto"
              alt="HomeImage"
              src={
                "https://beyondexams.org/static/media/home-top-illustration.97fcfd92.webp"
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeTopSection;
