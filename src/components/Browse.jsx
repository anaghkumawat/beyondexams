import axios from "axios";
import React, { useEffect, useState } from "react";
import CourseCard from "./CourseCard";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

const Browse = () => {
  const params = useParams();

  const [skillData, setSkillData] = useState([]);
  const [syllabusData, setSyllabusData] = useState([]);

  useEffect(() => {
    if (params.browse === "learn-a-skill") {
      axios
        .get(
          "https://api.beyondexams.org/api/v1/get_next_level_topics?level=1&parent_id=0"
        )
        .then((response) => {
          setSkillData(response.data.data.data);
        });
    } else {
      axios
        .get(
          "https://api.beyondexams.org/api/v1/get_courses?level=1&parent_id=0"
        )
        .then((response) => {
          setSyllabusData(response.data.data.courses.data);
        });
    }
  }, [params.browse]);

  return (
    <div className="my-8 flex flex-col px-[20px]">
      <div className="rounded-full shadow-[0_0_0_2px_#6646e7] flex items-center self-start">
        <Link to="/learn-a-skill">
          <button
            className={`px-[13px] py-[4px] duration-[.3s] text-[16px] font-medium min-w-[150px] ${
              params.browse === "learn-a-skill"
                ? "bg-[#6646e7] text-[white] rounded-full shadow-[0_0_0_1px_#6646e7]"
                : "text-[#6646e7]"
            }`}
          >
            Learn a skill
          </button>
        </Link>
        <Link to="/revise-your-syllabus">
          <button
            className={`px-[13px] py-[4px] duration-[.3s] text-[16px] font-medium min-w-[150px] ${
              params.browse === "revise-your-syllabus"
                ? "bg-[#6646e7] text-[white] rounded-full shadow-[0_0_0_1px_#6646e7]"
                : "text-[#6646e7]"
            }`}
          >
            Revise your syllabus
          </button>
        </Link>
      </div>
      <div className="flex flex-wrap gap-4 justify-evenly mt-[15px]">
        {params.browse === "learn-a-skill"
          ? skillData.length > 0
            ? skillData.map((course, index) => (
                <>
                  <Link
                    to={`/topics?level=${course.level + 1}&parent_id=${
                      course.id
                    }`}
                  >
                    <CourseCard
                      src={course.image_url}
                      title={course.title}
                      count={course.num_topics}
                      key={index}
                    />
                  </Link>
                </>
              ))
            : "Loading..."
          : syllabusData.length > 0
          ? syllabusData.map((course, index) => (
              <CourseCard
                src={course.image_url}
                title={course.title}
                count={course.num_categories}
                key={index}
              />
            ))
          : "Loading..."}
      </div>
    </div>
  );
};

export default Browse;
